# MIDI to Point Set Converter

This Python library converts MIDI files to point set representation, which
encodes notes as pairs of integers encoding the pitch and time. The output can
be used for machine learning tasks such as music generation.

## Requirements

- Python 3.8 or higher

## Installation

```sh
pip install midi_point_set
```

## Usage

### CLI

```sh
usage: python -m midi_point_set [-h] --input-midi INPUT_MIDI [--output-json OUTPUT_JSON] [--output-plot OUTPUT_PLOT] [--show-plot]

Convert MIDI files to point set encoding.

options:
  -h, --help            show this help message and exit
  --input-midi INPUT_MIDI
                        MIDI file that will be converted.
  --output-json OUTPUT_JSON
                        Output JSON file path with the point set.
  --output-plot OUTPUT_PLOT
                        Output plot file path with the point set.
  --show-plot           Display an interactive plot of the point set.
```

### Library

```py
from midi_point_set import get_point_set

point_set = get_point_set("mozart.mid")
print(point_set[:5])
```
