"""midi-point-set converts MIDI files to point set representation."""
import pretty_midi
from pathlib import Path
from typing import NewType


# TODO: times should be integers
Time = NewType('Time', float)
Tick = NewType('Tick', int)
Pitch = NewType('Pitch', int)


def get_time_point_set(midi: Path) -> list[tuple[Time, Pitch]]:
    """Get the point set for the given MIDI."""
    midi_data = pretty_midi.PrettyMIDI(str(midi))
    point_set = list()
    for instrument in midi_data.instruments:
        if instrument.is_drum:
            raise ValueError("Drum instruments not supported!")
        for note in instrument.notes:
            point_set.append((note.start, note.pitch))
    return point_set


def get_tick_point_set(midi: Path) -> list[tuple[Tick, Pitch]]:
    """Get the point set for the given MIDI."""
    midi_data = pretty_midi.PrettyMIDI(str(midi))
    point_set = list()
    for instrument in midi_data.instruments:
        if instrument.is_drum:
            raise ValueError("Drum instruments not supported!")
        for note in instrument.notes:
            tick = int(midi_data.time_to_tick(note.start))
            point_set.append((tick, note.pitch))
    return point_set
